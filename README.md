# discrete QGAN paper suplement

Github repository to reproduce the Figures of [Towards a scalable discrete quantum generative adversarial neural network](https://arxiv.org/abs/2209.13993).

The jupyter notebooks describe the code in more detail.

## How to cite this material

10.5281/zenodo.7092260