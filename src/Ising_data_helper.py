import numpy as np
import networkx as nx
import typing
from typing import Optional
from torch.utils.data import Dataset
from binary_data_helper import dec2bin

I = np.array([[1.0,0.0],[0.0,1.0]], dtype='float')
sx = np.array([[0.0,1.0],[1.0,0.0]], dtype='float')
sy = np.array([[0.0,-1.0j],[1.0j,0.0]], dtype='complex')
sz = np.array([[1.0,0.0],[0.0,-1.0]], dtype='float')
up = np.array([1.0,0.0])
down = np.array([0.0,1.0])
yup = np.array([1.0,1.0j])
ydown = np.array([0.0,-1.0j])
xup = np.array([1.0,1.0])
xdown = np.array([0.0,-1.0])

def ZZ(i: int,
       j: int,
       N: int):
    """Matrix representation of ZZ interaction of spins at position i and j 
    In a Hilbert space of size 2^N.

    Args:
        i (int): site index of first operator
        j (int): site index of 2nd operator
        N (int): nr of total spins

    Returns:
        _type_: ZZ operator in 2^N dim Hilbert space
    """
    result = 1.0
    for x in range(N):
        if x==i or x==j:
            result= np.kron(result, sz)
        else:
            result= np.kron(result, I)
    return result

def Z(i: int,
      N: int):
    """Matrix representation of a Z operator acting on spin i in a
    Hilbert space of size 2^N.

    Args:
        i (int): site index of the operator
        N (int): nr of total spins

    Returns:
        _type_: Z operator in 2^N dim Hilbert space
    """
    result= 1.0
    for j in range(N):
        if j==i:
            result= np.kron(result,sz)
        else:
            result= np.kron(result,I)
    return result

def get_Hamiltonian(N: int, 
                    J_list: list[list[float, tuple[int, int]]]):
    """Create Hamiltonian from the interaction graph J_list

    Args:
        N (int): nr of total spins
        J_list (list[list[float, tuple[int, int]]]): Interaction graph with list of lists of [coupling, (index 1, index 2)]

    Returns:
        _type_: Hamiltonian in 2^N dim Hilbert space
    """
    ham= 0.0
    for Jij, e in J_list:
        ham+= Jij*ZZ(e[0],e[1],N)
    return ham

def generate_1D_chain_graph(N: int, 
                            couplings: any = 1) -> list[list[float, int, int]]:
    """Generate a 1D chain Hamiltonian. The couplings can either be all equal or 
    a list. The list must be N-1 dimensional.

    Args:
        N (int): Length of chain
        couplings (int, optional): list or single value of coulings. If only given
                                   a single value, all the coulings are the same. If given
                                   a list, it must be of length N-1. Defaults to 1.

    Returns:
        (list[list[float, (int, int)]]): list of lists containing the couplings and the edges
    """
    g = nx.grid_graph(dim = [1, N], periodic = False)
    edges = list(g.edges)
    position_dict = dict( (n, i) for i, n in enumerate(g.nodes()) )

    if isinstance(couplings, typing.Iterable): # Check if couplings is iterable
        if not len(edges) == len(couplings):
            raise ValueError(f'The given couplings have the wrong dimension. There are {len(edges)} couplings in this graph, but the couplings list has length {len(couplings)}')
    else:
        couplings = [couplings]*len(edges)
    
    weights_and_edges = []
    for i,e in enumerate(edges):
        weights_and_edges.append([couplings[i], (position_dict[e[0]], position_dict[e[1]])])
    return weights_and_edges


def generate_2D_grid_graph(N: int, 
                          couplings: any = 1) -> list[list[float, int, int]]:
    """Generate a 2D grid Hamiltonian. The couplings can either be all equal or 
    a list. The list must be N^2 dimensional.

    Args:
        N (int): Size of 2D square grid
        couplings (int, optional): list or single value of coulings. If only given
                                   a ingle value, all the coulings are the same. If given
                                   a list, it must be of length N^2. Defaults to 1.

    Returns:
        (list[list[float, (int, int)]]): list of lists containing the couplings and the edges
    """
    g = nx.grid_2d_graph(N, N, periodic = False)
    edges = list(g.edges)
    position_dict = dict( (n, i) for i, n in enumerate(g.nodes()) )

    if isinstance(couplings, typing.Iterable): # Check if couplings is iterable
        if not len(edges) == len(couplings):
            raise ValueError(f'The given couplings have the wrong dimension. There are {len(edges)} couplings in this graph, but the couplings list has length {len(couplings)}')
    else:
        couplings = [couplings]*len(edges)
    
    weights_and_edges = []
    for i,e in enumerate(edges):
        weights_and_edges.append([couplings[i], (position_dict[e[0]], position_dict[e[1]])])
    return weights_and_edges


def generate_random_graph(N: int, 
                          seed: int = 42) -> list[list[float, int, int]]:
    """Generating a random fully connected graph

    Args:
        N (int): _description_
        seed (int, optional): _description_. Defaults to 42.

    Returns:
        list[list[float, int, int]]: returns a list of lists, containing [coupling, node1, node2]
    """
    np.random.seed(seed)
    g = nx.complete_graph(N)
    edges = list(g.edges)
    weights_and_edges = []
    for e in edges:
        weights_and_edges.append([(np.random.rand()-0.5)*2, e])
    return weights_and_edges

def explicit_Hamiltonian_diagonal(weights_and_edges: list, N: int) -> np.array:
    """
    We represent the classical spin Hamiltonian as a adjacency matrix, 
    so we can later calculate <H0> = \sigma^T H0 \sigma from samples \sigma.
    """
    H0 = np.zeros((N,N))
    for Jij, (i,j) in weights_and_edges:
        H0[i,j] = Jij
    return H0


class IsingDataset(Dataset):
    """Pytorch dataloader for the Ising data

    Args:
        auxiliary_G (int): nr of auxiliaries in G
        auxiliary_D (int): nr of auxiliaries in D
        n_work (int): nr of qubits used for data encoding
        transform (Optional[any], optional): Pytorch transformation. Defaults to None.
        folder (Optional[str], optional): Directory to load Ising data. Defaults to "training_data_sets/".
        graph_type (str, optional): What graph type should be loaded. Defaults to "1D".
        seed (int, optional): random seed. Defaults to 42.
        max_data (int, optional): how many states should be loaded. Defaults to 100.
        add_zeros_to_auxiliary (bool, optional): Patch the data with zeros on auxiliary qubits. Defaults to False.
    """
    def __init__(self, 
                auxiliary_G: int, 
                auxiliary_D: int, 
                n_work: int,
                transform: Optional[any]=None, 
                folder: Optional[str]= "training_data_sets/",
                graph_type = "1D", 
                seed = 42, 
                max_data = 100,
                add_zeros_to_auxiliary = False):


        self.transform = transform
        self.states = np.loadtxt(f"{folder}Ising_states_{graph_type}_N{n_work}_seed{seed}.csv", delimiter=",")
        self.energies = np.loadtxt(f"{folder}Ising_energies_{graph_type}_N{n_work}_seed{seed}.csv", delimiter=",")
        nr_states = np.min(np.array([max_data, (2**n_work)//2]))

        # add zeros to auxiliary system qubits such that data is only fed to working qubits
        shape = self.states.shape
        if add_zeros_to_auxiliary:
            X = np.zeros((shape[0], n_work + auxiliary_D + auxiliary_G))
            X[:, auxiliary_G:n_work+auxiliary_G] = self.states
        else:
            X = self.states

        self.df = X[:nr_states]
        self.df_energy = self.energies[:nr_states]

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        image = self.df[idx]
        energy = self.df_energy[idx]
        image = image.astype(np.float64)
        return image, energy

class IsingDataset_for_discriminator(Dataset):
    """Pytorch dataloader for the Ising data for discriminator training

    Args:
        auxiliary_G (int): nr of auxiliaries in G
        auxiliary_D (int): nr of auxiliaries in D
        n_work (int): nr of qubits used for data encoding
        transform (Optional[any], optional): Pytorch transformation. Defaults to None.
        folder (Optional[str], optional): Directory to load Ising data. Defaults to "training_data_sets/".
        graph_type (str, optional): What graph type should be loaded. Defaults to "1D".
        seed (int, optional): random seed. Defaults to 42.
        max_data (int, optional): how many states should be loaded. Defaults to 100.
        add_zeros_to_auxiliary (bool, optional): Patch the data with zeros on auxiliary qubits. Defaults to False.
    """

    def __init__(self, 
                auxiliary_G: int, 
                auxiliary_D: int, 
                n_work: int,
                transform: Optional[any]=None, 
                folder: Optional[str]= "training_data_sets/",
                graph_type = "1D",
                nr_high_energy_states = 8, 
                seed = 42, 
                max_data = 100, 
                add_zeros_to_auxiliary = False):
                
        self.transform = transform
        self.states = np.loadtxt(f"{folder}Ising_states_{graph_type}_N{n_work}_seed{seed}.csv", delimiter=",")
        self.energies = np.loadtxt(f"{folder}Ising_energies_{graph_type}_N{n_work}_seed{seed}.csv", delimiter=",")
        self.max_data = np.min((max_data, 2**n_work))
        self.nr_high_energy_states = nr_high_energy_states
        
        self.nr_low_energy_states = self.max_data - nr_high_energy_states

        # add zeros to auxiliary system qubits such that data is only fed to working qubits
        shape = self.states.shape
        if add_zeros_to_auxiliary:
            X = np.zeros((shape[0], n_work + auxiliary_D + auxiliary_G))
            X[:, auxiliary_G:n_work+auxiliary_G] = self.states
        else:
            X = self.states

        self.df = np.concatenate((X[:nr_high_energy_states],X[-self.nr_low_energy_states:]))
        self.df_energy = np.concatenate((self.energies[:nr_high_energy_states], self.energies[-self.nr_low_energy_states:]))
        self.y = np.zeros(self.nr_low_energy_states + nr_high_energy_states)
        self.y[self.nr_high_energy_states:] = 1 # high energy is 0, low energy is 1

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        image = self.df[idx]
        labels = self.y[idx]
        image = image.astype(np.float64)
        return image, labels


if __name__ == "__main__":
    L=8
    seed = 42
    np.random.seed(seed)
    J_list = np.random.uniform(0,2, L-1)
    
    graph_type = "1D"

    if graph_type == "1D":
        weights_and_edges = generate_1D_chain_graph(L, couplings = J_list)
    elif graph_type == "2D":
        weights_and_edges = generate_2D_grid_graph(L, couplings = 1)
    elif graph_type == "random":
        weights_and_edges = generate_random_graph(L, seed = seed)
    else:
        raise ValueError("graph_type not defined")

    H= get_Hamiltonian(L, weights_and_edges)
    vals, vecs= np.linalg.eigh(H)

    V = vecs.T # transpose because of numpy eigh convention psi_0 = vecs[:, 0] 
    indices = np.where(V == 1 ) # check where the eigenstates are 1.
    decimal_states = indices[1] # If an eigenstate is 1 at index n, then the corresponding spin config is n in binary representation
    binary_states = dec2bin(decimal_states, L)

    # Sanity check: Do these spin configs correspond to the right eigenenergies.
    sigma = (binary_states-0.5)*2
    H_adjacency = explicit_Hamiltonian_diagonal(weights_and_edges, L)
    eigenvalues = np.einsum('ij,jk,ik->i', sigma, H_adjacency, sigma)
    print(f"If this sum is zero, then everything went well. Sum: {(eigenvalues - vals).sum()}")

    np.savetxt(f"training_data/Ising_states_{graph_type}_N{L}_seed{seed}.csv", binary_states, delimiter=",")
    np.savetxt(f"training_data/Ising_energies_{graph_type}_N{L}_seed{seed}.csv", vals, delimiter=",")
    np.save(f"training_data/Ising_Weights_and_Edges_{graph_type}_N{L}_seed{seed}.npy", weights_and_edges)

    binary_states2 = np.loadtxt(f"training_data/Ising_states_{graph_type}_N{L}_seed{seed}.csv", delimiter=",")
    vals2 = np.loadtxt(f"training_data/Ising_energies_{graph_type}_N{L}_seed{seed}.csv", delimiter=",")
    weights_and_edges2 = np.load(f"training_data/Ising_Weights_and_Edges_{graph_type}_N{L}_seed{seed}.npy", allow_pickle=True)