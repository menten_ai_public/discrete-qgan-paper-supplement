import numpy as np

def dec2bin(x: np.array, 
            bits: int) -> np.array: 
    """converts decimal np.array to binary

    Args:
        x (np.array): _description_
        bits (int): _description_

    Returns:
        _type_: _description_
    """
    mask = 2 ** np.arange(bits - 1, -1, -1)
    y = np.expand_dims(x, axis=1)
    y = np.bitwise_and(y, mask)
    out = (y>0)*1.
    return out