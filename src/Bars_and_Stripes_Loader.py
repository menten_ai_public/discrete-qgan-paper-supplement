import numpy as np
from torch.utils.data import Dataset, DataLoader
from .binary_data_helper import dec2bin
from typing import Optional


def BAS(size: Optional[tuple]=(4,4)):
    """
    Generate B&S dataset
    """
    all_data = []
    for i in range(size[0]):
        bars = np.zeros(shape=size)
        stripes = np.zeros(shape=size)
        bars[:,i%size[1]] = 1.
        stripes[i%size[0],:] = 1.
        all_data.append(bars)
        all_data.append(stripes)

    bars = np.ones(shape=size)
    stripes = np.zeros(shape=size)
    all_data.append(bars)
    all_data.append(stripes)
    return np.array(all_data), np.zeros(len(all_data))


def generate_noise_data(n_work: int, 
                        bs_data: np.array):
    """Generate all datasets except the one from the data

    Args:
        n_work (int): nr of working qubits
        bs_data (np.array): data

    Returns:
        _type_: _description_
    """
    x = dec2bin(np.arange(0,2**n_work), n_work)
    indices = np.flip(2**np.arange(0,n_work))
    x_dec = (x * indices).sum(axis=1)
    data_dec = (bs_data.reshape(-1, n_work)*indices).sum(axis=1)
    noise_dec = np.setdiff1d(x_dec, data_dec)
    noise_bin = dec2bin(noise_dec.astype(int), n_work)
    noise_data = noise_bin.reshape((-1, n_work//2, n_work//2))
    return noise_data, np.ones(len(noise_data))


class BASDataset(Dataset):
    """B&S dataloader"""

    def __init__(self, 
                transform: Optional[any]=None, 
                size: Optional[tuple]=(4,4)):
            
        self.transform = transform
        self.df, _ = BAS(size)

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        image = self.df[idx]
        # image = np.array(image)
        image = image.astype(np.float64)

        # Return image and label
        return image, 0

class BASDataset_for_discrimination(Dataset):
    """dataloader that contains B&S with label 0, and noise with label 1"""

    def __init__(self, 
                transform: Optional[any]=None, 
                size: Optional[tuple]=(4,4)):
                
        self.transform = transform
        self.df_bs, self.y_bs = BAS(size)
        self.df_noise, self.y_noise = generate_noise_data(np.prod(size), self.df_bs)
        self.df = np.concatenate((self.df_bs, self.df_noise))
        self.y = np.concatenate((self.y_bs, self.y_noise))

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        image = self.df[idx]
        image = image.astype(np.float64)
        label = self.y[idx]
        return image, label
    


def numpy_collate(batch):
    if isinstance(batch[0], np.ndarray):
        return np.stack(batch)
    elif isinstance(batch[0], (tuple,list)):
        transposed = zip(*batch)
        return [numpy_collate(samples) for samples in transposed]
    else:
        return np.array(batch)

class NumpyLoader(DataLoader):
    def __init__(self, dataset, batch_size=1,
                shuffle=False, sampler=None,
                batch_sampler=None, num_workers=0,
                pin_memory=False, drop_last=False,
                timeout=0, worker_init_fn=None):
        super(self.__class__, self).__init__(dataset,
            batch_size=batch_size,
            shuffle=shuffle,
            sampler=sampler,
            batch_sampler=batch_sampler,
            num_workers=num_workers,
            collate_fn=numpy_collate,
            pin_memory=pin_memory,
            drop_last=drop_last,
            timeout=timeout,
            worker_init_fn=worker_init_fn)
