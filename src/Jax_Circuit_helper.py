import numpy as np
import pennylane as qml
import jax.numpy as jnp
from flax import linen as nn
from typing import Any, Sequence, Optional, Iterable


def circuit_D(weights: jnp.array, 
              depth_D: int, 
              n_work: int, 
              auxiliary_D: int, 
              auxiliary_G: int):
    """Function to construct the discriminator circuit

    Args:
        weights (jnp.array): circuit parameters. Should have shape (depth_D, n_qubits, 3)
        depth_D (int): depth of the circuit (nr of layers)
        n_work (int): nr of qubits used for data encoding
        auxiliary_D (int): nr of auxiliaries in D
        auxiliary_G (int): nr of auxiliaries in G
    """
    n_qubits = n_work + auxiliary_D
    for i in range(depth_D):
        for j, k in enumerate(range(auxiliary_G, n_qubits + auxiliary_G)):
            qml.Rot(*weights[i][j], wires=k)
        for k in range(auxiliary_G, n_qubits + auxiliary_G - 1):
            qml.CZ(wires=[k, k + 1])

def circuit_to_execute_D(x: jnp.array, 
                         weights: jnp.array, 
                         depth_D: int, 
                         n_work: int, 
                         auxiliary_D: int):
    """This function is only for the data loading. I.e. no auxiliary G required, because they are 
    anyway ignored by the discriminator.

    Args:
        x (jnp.array): input data
        weights (jnp.array): circuit parameters. Should have shape (depth_D, n_qubits, 3)
        depth_D (int): depth of the circuit (nr of layers)
        n_work (int): nr of qubits used for data encoding
        auxiliary_D (int): nr of auxiliaries in D

    Returns:
        _type_: <Z> Expectation Value of the 0th qubit
    """
    for j, i in enumerate(range(n_work)):
        qml.RX(jnp.pi*x[j], wires = i)
    circuit_D(weights, depth_D, n_work, auxiliary_D, auxiliary_G = 0)
    return qml.expval(qml.PauliZ(0)) 


def Full_Model(state_G: jnp.array, 
               weights_D: jnp.array, 
               depth_D: int, 
               n_work: int, 
               auxiliary_D: int, 
               auxiliary_G: int):
    """_summary_

    Args:
        state_G (jnp.array): output state of the G circuit
        weights_D (jnp.array): D circuit parameters. Should have shape (depth_D, n_qubits, 3)
        depth_D (int): depth of the circuit (nr of layers)
        n_work (int): nr of qubits used for data encoding
        auxiliary_D (int): nr of auxiliaries in D
        auxiliary_G (int): nr of auxiliaries in G

    Returns:
        _type_: _description_
    """
    qml.AmplitudeEmbedding(features=state_G, wires=range(n_work + auxiliary_G), normalize=True)
    circuit_D(weights_D, depth_D, n_work, auxiliary_D, auxiliary_G)
    return qml.expval(qml.PauliZ(auxiliary_G))

def sample_G(state_G: jnp.array, 
             subkey: jnp.array, 
             shots: int, 
             n_total: int): 
    """Circuit to sample the state \psi_G

    Args:
        state_G (jnp.array): output state of the G circuit
        subkey (jnp.array): pseudo random number genartor key
        shots (int): nr of samples taken at measurement
        n_total (int): total number of qubits of G. (i.e. n_work + auxiliary_G)

    Returns:
        _type_: _description_
    """
    dev = qml.device("default.qubit.jax", wires=n_total, 
                     shots = shots, prng_key=subkey)
    @qml.qnode(dev, interface="jax")
    def circuit_to_sample_G(state_G, n_total):
        qml.AmplitudeEmbedding(features=state_G, wires=range(n_total), normalize=True)
        return qml.sample()
    return circuit_to_sample_G(state_G, n_total)

class ExplicitMLP(nn.Module):
    """
    Neural Network

    Args:
        features (Sequence[int]): list of dimension [input_dim, hidden_dim, ..., output_dim]
    """
    features: Sequence[int]

    def setup(self, epsilon = 1e-10):
        self.layers = [nn.Dense(feat) for feat in self.features]
        self.epsilon = epsilon

    def __call__(self, inputs):
        x = inputs
        for i, lyr in enumerate(self.layers):
            x = lyr(x)
            if i != len(self.layers) - 1:
                x = nn.relu(x)
        return nn.tanh(x) + self.epsilon


def reuploading_layer(z: jnp.array, 
                      weights: jnp.array, 
                      n_total: int, 
                      layer_idx: Optional[int] = 0):
    """A layer for the noise reuploading circuit with additional weights. 
    Consisting of a layer of Rx gates with the noise as an argument
    Followed by Rot(a,b,c) where a,b,c are trainable parameters
    Followed by a layer of CZ gates.

    Args:
        z (jnp.array): noise input
        weights (jnp.array): circuit parameters with shape (depth, n_qubits, 3)
        n_total (int): total qubits of circuit
        layer_idx (Optional[int], optional): index of the layer. Defaults to 0.
    """
    for i in range(n_total):
        qml.RX(jnp.pi*z[i], wires = i)
    for i in range(n_total):
        qml.Rot(*weights[layer_idx][i], wires=i)
    for i in range(n_total - 1):
        qml.CZ(wires=[i, i + 1])

def extended_reuploading_layer(z: jnp.array, 
                               weights: jnp.array, 
                               n_total: int, 
                               layer_idx: Optional[int] = 0):
    """A layer for the noise reuploading circuit with additional weights to be multiplied with the noise. 
    Consisting of a layer of Rot(W*z)
    Followed by Rot(a,b,c) where a,b,c are trainable parameters
    Followed by a layer of CZ gates.

    Args:
        z (jnp.array): noise input
        weights (jnp.array): circuit parameters with shape (depth, n_qubits, 4, 3)
                             The matrix W = weights[idx][qb][0:3]
                             And the "normal" weights are weight = weights[idx][qb][3]
        n_total (int): total qubits of circuit
        layer_idx (Optional[int], optional): index of the layer. Defaults to 0.
    """
    for i in range(n_total):
        W = jnp.pi*weights[layer_idx][i][0:3].dot(z)
        qml.Rot(*W, wires = i)
    for i in range(n_total):
        qml.Rot(*weights[layer_idx][i][3], wires=i)
    for i in range(n_total - 1):
        qml.CZ(wires=[i, i + 1])

def weights_layer(weights: jnp.array, 
                  n_total: int, 
                  layer_idx = 0):
    """A layer of parameterized gates
    Consiting of Rot(a,b,c) where a,b,c are trainable parameters
    Followed by a layer of CZ gates.

    Args:
        weights (jnp.array): circuit parameters with shape (depth, n_qubits, 3)
                             The matrix W = weights[idx][qb][0:3]
                             And the "normal" weights are weight = weights[idx][qb][3]
        n_total (int): total qubits of circuit
        layer_idx (Optional[int], optional): index of the layer. Defaults to 0.
    """
    for i in range(n_total):
        qml.Rot(*weights[layer_idx][i], wires=i)
    for i in range(n_total - 1):
        qml.CZ(wires=[i, i + 1])

def extended_QNN(z: jnp.array, 
                 weights: jnp.array, 
                 n_layers: int, 
                 n_total: int):
    """QNN circuit with reuploading and the extended reuploading layer
    where the noise input is pultiplied with a matrix W 

    Args:
        z (jnp.array): noise input
        weights (jnp.array): circuit parameters with shape (depth, n_qubits, 4, 3)
                             The matrix W = weights[idx][qb][0:3]
                             And the "normal" weights are weight = weights[idx][qb][3]
        n_total (int): total qubits of circuit
        n_layers (int): nr of layers in the circuit

    Returns:
        _type_: ouput state \psi_G
    """
    for i in range(n_layers):
        extended_reuploading_layer(z, weights, n_total, layer_idx = i)
    return qml.state()

def QNN(z: jnp.array, 
        weights: jnp.array, 
        n_layers: int, 
        n_total: int):
    """QNN circuit with reuploading

    Args:
        z (jnp.array): noise input
        weights (jnp.array): circuit parameters with shape (depth, n_qubits, 3)
        n_total (int): total qubits of circuit
        n_layers (int): nr of layers in the circuit

    Returns:
        _type_: ouput state \psi_G
    """
    for i in range(n_layers):
        reuploading_layer(z, weights, n_total, layer_idx = i)
    return qml.state()

def linear_QNN(z: jnp.array, 
               weights: jnp.array, 
               n_layers: int, 
               n_total: int):
    """QNN circuit without reuploading, just with one layer of data loading at the
    beginning of the circuit, followed by paramterized gates.

    Args:
        z (jnp.array): noise input
        weights (jnp.array): circuit parameters with shape (depth, n_qubits, 3)
        n_total (int): total qubits of circuit
        n_layers (int): nr of layers in the circuit

    Returns:
        _type_: ouput state \psi_G
    """
    reuploading_layer(z, weights, n_total, layer_idx = 0)
    for i in range(n_layers-1):
        weights_layer(weights, n_total, layer_idx = i)
    return qml.state()


def get_percentage_of_samples(state_G, subkey, n_samples, n_work, auxiliary_G = 0):
    if any(isinstance(el, Iterable) for el in np.array(state_G)): 
        # Check if there is a batch of states_G or just a single one
        # state_G needs to be converted to np.array because jnp.DeviceArray
        # is iterable even if it's scalar
        pass
    else:
        state_G = [state_G]
    list_of_indices = []
    basis_idx = 2**jnp.arange(0, n_work)[::-1]
    for state in state_G:
        samples = sample_G(state, subkey, n_samples, n_work + auxiliary_G)
        S = samples[:, auxiliary_G:n_work + auxiliary_G]
        idx = (S*basis_idx).sum(axis=1)
        list_of_indices.append(idx)
    final_indices = jnp.concatenate(list_of_indices)

    percentage = []
    for i in range(2**n_work):
        p = (final_indices == i).sum()/(n_samples*len(state_G))
        percentage.append(p.item())
    return percentage

def get_percentage_of_dataset(data, n_work, auxiliary_G = 0):
    basis_idx = 2**jnp.arange(0, n_work)[::-1]
    S = data[:, auxiliary_G:n_work+auxiliary_G]
    indices = (S*basis_idx).sum(axis=1)
    percentage = []
    for i in range(2**n_work):
        p = (indices == i).sum()/(len(data))
        percentage.append(p.item())
    return percentage
